<?php

namespace App\Http\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class CoreRepositories
{
    protected $model;

    public function __construct()
    {
        $this->model = app($this->getModelClass());
    }

    abstract protected function getModelClass();

    protected function startCondition()
    {
        return clone $this->model;
    }
}
