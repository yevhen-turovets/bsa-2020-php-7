<?php

namespace App\Http\Repositories;

use App\Entity\Product as Model;

class ProductRepository extends CoreRepositories
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getEdit($id)
    {
        return $this->startCondition()->find($id);
    }
}
