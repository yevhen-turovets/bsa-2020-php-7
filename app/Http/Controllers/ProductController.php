<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Http\Repositories\ProductRepository;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
    }

    public function index()
    {
        $products = Product::all()->sortByDesc('updated_at');

        return view('products.index', compact('products'));
    }

    public function create()
    {
        $product = new Product();

        return view('products.create', compact('product'));
    }

    public function store(ProductCreateRequest $request)
    {
        $data = $request->input();

        $data['user_id'] = Auth::id();

        $product = (new Product())->create($data);

        if ($product) {
            return redirect()
                ->route('products.index')
                ->with(['success' => 'Successfully created']);
        } else {
            return back()->withErrors(['msg' => 'Creation error'])->withInput();
        }
    }

    public function show($id)
    {
        $product = Product::find($id);

        try {
            $this->authorize('view', $product);
        } catch (AuthorizationException $e) {
            return back()
                ->withErrors(['msg' => 'You don\'t have permission to view this product'])
                ->withInput();
        }

        return view('products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->productRepository->getEdit($id);

        if (empty($product)) {
            abort(404);
        }

        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return back()
                ->withErrors(['msg' => 'You don\'t have permission to edit this product'])
                ->withInput();
        }

        return view('products.edit', compact('product'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $this->productRepository->getEdit($id);

        if (empty($product)) {
            return back()
                ->withErrors(['msg' => "Product with id=[{$id}] not found"])
                ->withInput();
        }

        try {
            $this->authorize('update', $product);
        } catch (AuthorizationException $e) {
            return back()
                ->withErrors(['msg' => 'You don\'t have permission to update this product'])
                ->withInput();
        }

        $data = $request->input();

        $result = $product->update($data);

        if ($result) {
            return redirect()
                ->route('products.show', $product->id)
                ->with(['success' => 'Successfully updated']);
        } else {
            return back()
                ->withErrors(['msg' => 'Updated error'])
                ->withInput();
        }
    }

    public function destroy($id)
    {
        $product = $this->productRepository->getEdit($id);

        if (empty($product)) {
            return back()
                ->withErrors(['msg' => "Product with id=[{$id}] not found"])
                ->withInput();
        }

        try {
            $this->authorize('delete', $product);
        } catch (AuthorizationException $e) {
            return back()
                ->withErrors(['msg' => 'You don\'t have permission to delete this product'])
                ->withInput();
        }

        $result = Product::destroy($id);

        if ($result) {
            return redirect()
                ->route('products.index')
                ->with(['success' => "Product with id=$id deleted"]);
        } else {
            return back()
                ->withErrors(['msg' => 'Deleted error'])
                ->withInput();
        }
    }
}
