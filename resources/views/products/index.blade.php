@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                @include('products.includes.result_messages')

                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" href="{{ route('products.create') }}">Add</a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Author</th>
                            </tr>
                            </thead>
                            <tbody>

                                @foreach($products as $product)
                                    <tr @if(Auth::id() == $product->user_id)
                                            style="background-color: #ccc;"
                                        @endif
                                        >
                                        <td>
                                            {{ $product->id }}
                                        </td>
                                        <td>
                                            <a href="{{ route('products.show', $product->id) }}">
                                                {{ $product->name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $product->price }}
                                        </td>
                                        <td>
                                            {{ $product->author->name }}
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
