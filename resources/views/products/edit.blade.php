@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('products.update', $product->id) }}">
        @method('PATCH')
        @csrf
        <div class="container">

            @include('products.includes.result_messages')

            <div class="row justify-content-center">
                <div class="col-md-9">
                    @include('products.includes.item_edit_main_col')
                </div>
                <div class="col-md-3">
                    @include('products.includes.item_edit_add_col')
                </div>
            </div>
        </div>
    </form>
@endsection
