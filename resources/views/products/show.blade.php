@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                @include('products.includes.result_messages')

                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-sm-10">
                                <h4>{{ $product->name }}</h4>
                            </div>
                            <div class="col-sm-1">

                                @can('update', $product)
                                    <a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}">
                                        Edit
                                    </a>
                                @endcan

                            </div>
                            <div class="col-sm-1">

                                @can('delete', $product)
                                    <form id="delete-{{ $product->id }}" action="{{ route('products.destroy', $product->id) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                @endcan

                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul>
                            <li>ID: {{ $product->id }}</li>
                            <li>Name: {{ $product->name }}</li>
                            <li>Price: {{ $product->price }}</li>
                            <li>Author: {{ $product->author->name }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
