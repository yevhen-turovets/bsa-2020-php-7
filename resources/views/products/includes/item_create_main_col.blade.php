<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title"><h4>Create new product</h4></div>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}"
                               id="name"
                               class="form-control"
                               minlength="3"
                               required
                        >
                    </div>
                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="text" name="price" value="{{ old('price') }}"
                               id="price"
                               class="form-control"
                               required
                        >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
